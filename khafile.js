let project = new Project('New Project');
project.addAssets('Assets/**', {
    nameBaseDir: 'Assets',
    destination: '{dir}/{name}',
    name: '{dir}/{name}'
});
project.addShaders('Shaders/**');
project.addSources('Sources');
project.addLibrary('Library/zui');
// project.addLibrary('Library/haxe-gltf');
project.addLibrary('Library/hx-glTF');
// project.addLibrary('Library/iron_format');
project.addLibrary('tink_lang')
resolve(project);