package engine;

import engine.Object;

class ObjectManager {

    // list/array
    var objects:Array<Object>;

    public function new(?arr:Array<Object>) {
        this.objects = (arr != null) ? arr : [];
    }

    public function update() {
        for (obj in this.objects) obj.update();
    }

    public function add(object:Object) {
        this.objects.push(object);
    }

    public function get() : Array<Object> {
        return this.objects;
    }
}