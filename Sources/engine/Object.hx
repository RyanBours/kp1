package engine;

import engine.helpers.GLTFModel;
import engine.Mesh;
import kha.math.Vector3;

class Object {
	// TODO: Refactor class to ecs object
	public var translate:Vector3;
	public var rotate:Vector3;
	public var scale:Vector3;

	public var mesh:Mesh;
	// public ?var collider:Array<Vector3>;

	var parentElement(default, default):Object;
	var childElement(default, default):Object;

	public function new(mesh:Mesh) {
		translate = new Vector3();
		rotate = new Vector3();
		scale = new Vector3(1, 1, 1);

		this.mesh = mesh;
	}

	public function update() {}

	public function getVertexBuffers() {
		return this.mesh.vbo;
	}

	public function getIndexBuffers() {
		return this.mesh.ibo;
	}

	public function getParentElements() {
		// TODO: implement
		/*
		return array of ALL preceding parentElements
		*/ 
	}

	public function getChildElements() {
		// TODO: implement
		/*
		return array of ALL preceding childElements
		*/ 
	}
}
