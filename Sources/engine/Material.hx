package engine;

import kha.graphics4.hxsl.Types.Sampler2D;

typedef Material = {
    var diffuse:Sampler2D,
    var specular:Sampler2D,
    var shininess:Float
}