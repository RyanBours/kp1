package engine;

import kha.graphics4.IndexBuffer;
import kha.graphics4.VertexBuffer;

class Mesh {
    public var vbo(default, null) : VertexBuffer;
    public var ibo(default, null) : IndexBuffer;
    
    public function new(vbo:VertexBuffer, ibo:IndexBuffer) {
        this.vbo = vbo;
        this.ibo = ibo;
    }
}