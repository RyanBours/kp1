package engine;

import kha.math.Vector3;

typedef Light = {};

typedef Directional = {
    >Light,
    var direction:Vector3, 
    var diffuse:Float, // ??
    var specular:Vector3
}

typedef Point = {
    >Light
    var position:Vector3,
    var rotation:Vector3,
    var intensity:Float,
    var color:Vector3
}

typedef Spot = {
    >Light,
    var position:Vector3,
    var intensity:Float,
    var radius:Float // radius/limit
    var color:Vector3
}