package engine.helpers;

import engine.Mesh;

import kha.graphics4.IndexBuffer;
import kha.graphics4.Usage;
import kha.graphics4.VertexBuffer;
import kha.graphics4.VertexData;
import kha.graphics4.VertexStructure;

class ObjModel {

    public var vertexBuffer:VertexBuffer;
	public var indexBuffer:IndexBuffer;

    // TODO: Refactor pos, uv & nor names
    public var pos = new Array<Float>(); // (x, y, z [,w]) coordinates, w is optional and defaults to 1.0.
    public var uv = new Array<Float>(); // (u, [,v ,w]) coordinates, these will vary between 0 and 1. v, w are optional and default to 0.
    public var nor = new Array<Float>(); // (x,y,z) form; normals might not be unit vectors.

    public var posIndices = new Array<Int>(); // f v1 v2 v3 ....
    public var uvIndices = new Array<Int>(); // f v1/vt1 v2/vt2 v3/vt3 ...
    public var norIndices = new Array<Int>(); // f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3 ...

    public function new(data : String) {
        // var items = ["a", "b", "c"];
        // for (index => item in items) { // BUG: Array<String> has no field keyValueIterator
            // trace('$index : $item');
        // }

        parse(data);

        // TODO: dynamic vertexStruct
        var structure = new VertexStructure();
		structure.add("pos", VertexData.Float3);
		structure.add("uv", VertexData.Float2);
        structure.add("nor", VertexData.Float3);
        
        createVertexBuffer(structure);
        createIndexBuffer(structure);
    }

    private function parse(data : String) {
        for (line in data.split("\n")) {
            switch (line.split(' ')[0]) {
                case "v":
                    // geometric vertices
                    var vert = line.split(' ');
                    // trace(vert);
                    pos.push(Std.parseFloat(vert[1]));
                    pos.push(Std.parseFloat(vert[2]));
                    pos.push(Std.parseFloat(vert[3]));
                    // pos.push((vert[4] != null) ? Std.parseFloat(vert[4]) : 1.0); // Vertex color

                case "vt":
                    // uvs
                    var vert = line.split(' ');
                    // trace(vert);
                    uv.push(Std.parseFloat(vert[1]));
                    uv.push((vert[2] != null) ? Std.parseFloat(vert[2]) : 0.0);
                    // uv.push((vert[3] != null) ? Std.parseFloat(vert[3]) : 0.0);
                case "vn":
                    // normals
                    var vert = line.split(' ');
                    // trace(vert);
                    nor.push(Std.parseFloat(vert[1]));
                    nor.push(Std.parseFloat(vert[2]));
                    nor.push(Std.parseFloat(vert[3]));
                case "f":
                    // indices
                    var items = line.split(' ');
                    for (index in 0...items.length) {
                        if (index == 0) continue;
                        var indices = items[index].split('/');

                        var tempVert = Std.parseInt((indices.length > 1) ? indices[0] : indices[0].substr(1)); // TODO: doublecheck
                        posIndices.push(tempVert - 1);

                        if (indices[1] != null)
                            uvIndices.push(Std.parseInt(indices[1]) - 1);

                        if (indices[2] != null)
                            norIndices.push(Std.parseInt(indices[2]) - 1);

                    }
            }
        }
    }

    private function createVertexBuffer(structure:VertexStructure) {
        // TODO: dynamic structure
        vertexBuffer = new VertexBuffer(Std.int(pos.length / 3), structure, Usage.StaticUsage);
        // vertexBuffer = new VertexBuffer(324, structure, Usage.StaticUsage);
        var structureLength:Int = Std.int(structure.byteSize() / 4);
        var vbData = vertexBuffer.lock();
        for (i in 0...vbData.length) {
            vbData[(i * structureLength) + 0] = pos[(i * 3) + 0];
            vbData[(i * structureLength) + 1] = pos[(i * 3) + 1];
            vbData[(i * structureLength) + 2] = pos[(i * 3) + 2];

            vbData[(i * structureLength) + 3] = uv[(i * 2) + 0];
            vbData[(i * structureLength) + 4] = 1-uv[(i * 2) + 1];

            vbData[(i * structureLength) + 5] = nor[(i * 3) + 0];
            vbData[(i * structureLength) + 6] = nor[(i * 3) + 1];
            vbData[(i * structureLength) + 7] = nor[(i * 3) + 2];
        }
        vertexBuffer.unlock();
    }

    private function createIndexBuffer(structure:VertexStructure) {
        indexBuffer = new IndexBuffer(posIndices.length, Usage.StaticUsage);
        var iData = indexBuffer.lock();
        for (i in 0...iData.length) {
            iData[i] = posIndices[i];
        }
        indexBuffer.unlock();
    }

    public function createMesh() : Mesh {
        var mesh = new Mesh(vertexBuffer, indexBuffer);
        return mesh;
    }
}