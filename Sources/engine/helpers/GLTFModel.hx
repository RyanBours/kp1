package engine.helpers;

import kha.graphics4.IndexBuffer;
import kha.graphics4.Usage;
import kha.graphics4.VertexStructure;
import kha.graphics4.VertexBuffer;
import haxe.ds.Vector;
import haxe.io.Bytes;
import gltf.GLTFLoader;

class GLTFModel {
	public var gltf:GLTF;

	public var VBOs:Vector<Vector<VertexBuffer>>;
	public var IBOs:Vector<Vector<IndexBuffer>>;

	var structure:VertexStructure;

	public function new(gltf:GLTF, structure:VertexStructure) {
		this.gltf = gltf;
		this.structure = structure;

		this.VBOs = this.createVertexBuffer();
		this.IBOs = this.createIndexBuffer();
	}

	public function createVertexBuffer(): Vector<Vector<VertexBuffer>> {
		var meshlist = new Vector(this.gltf.meshes.length);
		var primitiveslist;
		for (i in 0...this.gltf.meshes.length) {
			primitiveslist = new Vector(this.gltf.meshes[i].primitives.length);
			for (j in 0...this.gltf.meshes[i].primitives.length) {
				var vertex = this.getVertices(i, j);
				var vertexBuffer = new VertexBuffer(Std.int(vertex.pos.length / 3), this.structure, Usage.StaticUsage);
				// Copy data to vertex buffer
				var structureLength:Int = Std.int(this.structure.byteSize() / 4);
				var vbData = vertexBuffer.lock();
				for (i in 0...vbData.length) {
					vbData[(i * structureLength) + 0] = vertex.pos[(i * 3) + 0];
					vbData[(i * structureLength) + 1] = vertex.pos[(i * 3) + 1];
					vbData[(i * structureLength) + 2] = vertex.pos[(i * 3) + 2];
					if (vertex.uvs != null) {
						vbData[(i * structureLength) + 3] = vertex.uvs[(i * 2) + 0];
						vbData[(i * structureLength) + 4] = vertex.uvs[(i * 2) + 1];
					}
					if (vertex.nor != null) {
						vbData[(i * structureLength) + 5] = vertex.nor[(i * 3) + 0];
						vbData[(i * structureLength) + 6] = vertex.nor[(i * 3) + 1];
						vbData[(i * structureLength) + 7] = vertex.nor[(i * 3) + 2];
					}
				}
				vertexBuffer.unlock();
				primitiveslist[j] = vertexBuffer;
			}
			meshlist[i] = primitiveslist;
		}
		return meshlist;
	}

	public function createIndexBuffer() {
		var meshlist = new Vector(this.gltf.meshes.length);
		var primitiveslist;
		for (i in 0...this.gltf.meshes.length) {
			primitiveslist = new Vector(this.gltf.meshes[i].primitives.length);
			for (j in 0...this.gltf.meshes[i].primitives.length) {
				var indices:Vector<Int> = this.getIndices(i, j);

				// index buffer
				var indexBuffer = new IndexBuffer(indices.length, Usage.StaticUsage);
				// Copy indices to index buffer
				var iData = indexBuffer.lock();
				for (i in 0...iData.length) {
					iData[i] = indices[i];
				}
				indexBuffer.unlock();
				primitiveslist[j] = indexBuffer;
			}
			meshlist[i] = primitiveslist;
		}
		return meshlist;
	}

	public function getVertices(mesh:Int, prim:Int) {
		var prim = this.gltf.meshes[mesh].primitives[prim];
		var acces = this.gltf.accessors[prim.attributes.position];
		var bview = this.gltf.bufferViews[acces.bufferView];
		var bfer = this.gltf.buffers[bview.buffer];

		var data = cast(bfer.uri, Bytes).sub(bview.byteOffset, bview.byteLength); // vertex buffer data

		// pos
		var pos:Int = acces.byteOffset;
		var size:Int = switch (acces.type) {
			case DataType.Scalar: 1;
			case DataType.Vec2: 2;
			case DataType.Vec3: 3;
			case DataType.Vec4: 4;
			case DataType.Mat2: 4;
			case DataType.Mat3: 9;
			case DataType.Mat4: 16;
		};
		var positions:Vector<Float> = new Vector<Float>(acces.count * size);
		for (i in 0...(acces.count * size)) {
			positions[i] = data.getFloat(pos);
			pos += 4;
		}

		// uvs
		var uvs:Vector<Float> = new Vector(0);
		if (prim.attributes.texcoord_0 != null) {
			var acces = this.gltf.accessors[prim.attributes.normal];
			var bview = this.gltf.bufferViews[acces.bufferView];
			var bfer = this.gltf.buffers[bview.buffer];

			var data = cast(bfer.uri, Bytes).sub(bview.byteOffset, bview.byteLength); // vertex buffer data

			// vertex buffer data
			var pos:Int = acces.byteOffset;
			uvs = new Vector<Float>(acces.count * size);
			var size:Int = switch (acces.type) {
				case DataType.Scalar: 1;
				case DataType.Vec2: 2;
				case DataType.Vec3: 3;
				case DataType.Vec4: 4;
				case DataType.Mat2: 4;
				case DataType.Mat3: 9;
				case DataType.Mat4: 16;
			};
			for (i in 0...(acces.count * size)) {
				uvs[i] = data.getFloat(pos);
				pos += 4;
			}
		}

		// nor
		var normals:Vector<Float> = new Vector(0);
		if (prim.attributes.normal != null) {
			var acces = this.gltf.accessors[prim.attributes.normal];
			var bview = this.gltf.bufferViews[acces.bufferView];
			var bfer = this.gltf.buffers[bview.buffer];

			var data = cast(bfer.uri, Bytes).sub(bview.byteOffset, bview.byteLength); // vertex buffer data

			// vertex buffer data
			var pos:Int = acces.byteOffset;
			var size:Int = switch (acces.type) {
				case DataType.Scalar: 1;
				case DataType.Vec2: 2;
				case DataType.Vec3: 3;
				case DataType.Vec4: 4;
				case DataType.Mat2: 4;
				case DataType.Mat3: 9;
				case DataType.Mat4: 16;
			};
			normals = new Vector<Float>(acces.count * size);
			for (i in 0...(acces.count * size)) {
				normals[i] = data.getFloat(pos);
				pos += 4;
			}
		}

		var struct = {
			pos: positions,
			uvs: uvs,
			nor: normals
		};
		return struct;
	}

	public function getIndices(mesh:Int, prim:Int) {
		var prim = this.gltf.meshes[mesh].primitives[prim];
		var iacc = this.gltf.accessors[prim.indices];
		var iv = this.gltf.bufferViews[iacc.bufferView];
		var ibfer = this.gltf.buffers[iv.buffer];

		var _data = cast(ibfer.uri, Bytes).sub(iv.byteOffset, iv.byteLength); // index buffer data

		// indices data
		var pos:Int = iacc.byteOffset;
		var componentSize:Int = switch (iacc.type) {
			case DataType.Scalar: 1;
			case DataType.Vec2: 2;
			case DataType.Vec3: 3;
			case DataType.Vec4: 4;
			case DataType.Mat2: 4;
			case DataType.Mat3: 9;
			case DataType.Mat4: 16;
		};
		var indices:Vector<Int> = new Vector<Int>(iacc.count * componentSize);
		var accessor:Int->Void = switch (iacc.componentType) {
			case ComponentType.Byte | ComponentType.UnsignedByte: function(i:Int) {
					indices[i] = _data.get(pos);
					pos++;
				}
			case ComponentType.Short | ComponentType.UnsignedShort: function(i:Int) {
					indices[i] = _data.getUInt16(pos);
					pos += 2;
				}
			case ComponentType.UnsignedInt: function(i:Int) {
					indices[i] = _data.getInt32(pos);
					pos += 4;
				}
			case _: throw 'Floats aren\'t supported with this function!';
		}
		for (i in 0...(iacc.count * componentSize))
			accessor(i);

		return indices;
	}

	public function createMesh() : engine.Mesh {
		// TODO: return Array<Mesh>
		// returns first mesh only
		// @:return Mesh
		var mesh = new engine.Mesh(this.VBOs[0][0], this.IBOs[0][0]);
		return mesh;
	}
}
