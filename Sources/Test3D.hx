package;

import gltf.GLTFLoader;
import kha.math.Vector3;
import haxe.ds.Vector;
import zui.Id;
import zui.Zui;
import kha.graphics4.CullMode;
import kha.Scheduler;
import kha.input.KeyCode;
import kha.input.Mouse;
import kha.input.Keyboard;
import kha.Framebuffer;
import kha.Color;
import kha.Shaders;
import kha.Assets;
import kha.Image;
import kha.graphics4.TextureUnit;
import kha.graphics4.PipelineState;
import kha.graphics4.VertexStructure;
import kha.graphics4.VertexBuffer;
import kha.graphics4.IndexBuffer;
import kha.graphics4.VertexData;
import kha.graphics4.Usage;
import kha.graphics4.ConstantLocation;
import kha.graphics4.CompareMode;
import kha.math.FastMatrix4;
import kha.math.FastVector3;
import engine.Object;
import haxe.io.Bytes;
import engine.helpers.GLTFModel;
import engine.ObjectManager;

@:tink
class Test3D {
	// region Variables
	var ui:Zui;

	var vertexBuffer:VertexBuffer;
	var indexBuffer:IndexBuffer;

	var object_test:GLTFModel;

	var ObjectManager = new ObjectManager();

	var pipeline:PipelineState;

	var mvp:FastMatrix4;
	var mvpID:ConstantLocation;
	var viewMatrixID:ConstantLocation;
	var modelMatrixID:ConstantLocation;
	var translateID:ConstantLocation;
	var rotateID:ConstantLocation;
	var scaleID:ConstantLocation;
	var timeID:ConstantLocation;
	var viewPosID:ConstantLocation;

	// dirLight
	var dirLightDirID:ConstantLocation;
	var dirLightAmbID:ConstantLocation;
	var dirLightDiffID:ConstantLocation;
	var dirLightSpecID:ConstantLocation;

	// material
	var matDiffID:TextureUnit;
	var matSpecID:TextureUnit;
	var matShinID:ConstantLocation;

	var textureID:TextureUnit;

	var model:FastMatrix4;
	var view:FastMatrix4;
	var projection:FastMatrix4;

	var image_uv:Image;
	var image_devil:Image;

	var lastTime = 0.0; // delta
	var realTime = Scheduler.realTime();
	var lastRealTime = Scheduler.realTime(); // fps
	var fps:Float;

	var cameraPos:FastVector3 = new FastVector3(0, 5, 7.5); // Initial cameraPos: on +Z
	var horizontalAngle = 3.14; // Initial horizontal angle: toward -Z
	var verticalAngle = 0.0; // Initial vertical angle: none

	var moveForward = false;
	var moveBackward = false;
	var moveUp = false;
	var moveDown = false;
	var strafeLeft = false;
	var strafeRight = false;
	var isMouseDown = false;

	var mouseX = 0.0;
	var mouseY = 0.0;
	var mouseDeltaX = 0.0;
	var mouseDeltaY = 0.0;

	var speedBase = 6.0;
	var speed = 6.0; // 6 units / second
	var speedMod = 4.;
	var mouseSpeed = 0.005;

	// end region

	public function new() {
		// region structure
		var structure = new VertexStructure();
		structure.add("pos", VertexData.Float3);
		structure.add("uv", VertexData.Float2);
		structure.add("nor", VertexData.Float3);
		// end region

		// region pipeline
		pipeline = new PipelineState();
		pipeline.inputLayout = [structure];
		pipeline.vertexShader = Shaders.v2_vert;
		pipeline.fragmentShader = Shaders.v2_frag;
		pipeline.depthWrite = true;
		pipeline.depthMode = CompareMode.Less;
		pipeline.cullMode = CullMode.Clockwise;
		pipeline.compile();
		// end region

		// region MVP Matrix
		mvpID = pipeline.getConstantLocation("MVP");
		viewMatrixID = pipeline.getConstantLocation("V");
		modelMatrixID = pipeline.getConstantLocation("M");

		// lightID = pipeline.getConstantLocation("lightPos");
		textureID = pipeline.getTextureUnit("myTextureSampler");

		translateID = pipeline.getConstantLocation("translate");
		rotateID = pipeline.getConstantLocation("rotate");
		scaleID = pipeline.getConstantLocation("scale");

		timeID = pipeline.getConstantLocation("u_time");
		viewPosID = pipeline.getConstantLocation("viewPos");

		dirLightDirID = pipeline.getConstantLocation("dirLight.direction");
		dirLightAmbID = pipeline.getConstantLocation("dirLight.ambient");
		dirLightDiffID = pipeline.getConstantLocation("dirLight.diffuse");
		dirLightSpecID = pipeline.getConstantLocation("dirLight.specular");

		matDiffID = pipeline.getTextureUnit("material.diffuse");
		matSpecID = pipeline.getTextureUnit("material.specular");
		matShinID = pipeline.getConstantLocation("material.shininess");

		model = FastMatrix4.identity();
		projection = FastMatrix4.perspectiveProjection(45., Reference.SCREEN_WIDTH / Reference.SCREEN_HEIGHT, .1, 100.);
		view = FastMatrix4.lookAt(new FastVector3(4, 3, 3), new FastVector3(0, 0, 0), new FastVector3(0, 1, 0));

		mvp = FastMatrix4.identity();
		mvp = mvp.multmat(projection);
		mvp = mvp.multmat(view);
		mvp = mvp.multmat(model);
		// end region

		// var test = GLTFLoader.load(Assets.blobs.ship_wreck2_gltf.toString());
		var test = GLTFLoader.load(Assets.blobs.dodecadodecahedron_gltf.toString());
		// var test = GLTFLoader.load(Assets.blobs.minimal_gltf.toString());
		// var test = GLTFLoader.load(Assets.blobs.cube2_gltf.toString());
		// var test = GLTFLoader.load(Assets.blobs.test_gltf.toString()); // random character model
		// var test = GLTFLoader.load(Assets.blobs.joystick2_gltf.toString());

		object_test = new GLTFModel(test, structure);
		ObjectManager.add(new Object(object_test.createMesh()));

		// end region

		image_uv = Assets.images.uvmap;
		image_devil = Assets.images.textures_devil_Material_baseColor;

		Mouse.get().notify(onMouseDown, onMouseUp, onMouseMove, null);
		Keyboard.get().notify(onKeyDown, onKeyUp);

		// Used to calculate delta time
		lastTime = Scheduler.time();

		ui = new Zui({font: Assets.fonts.HackRegular});
	}

	public function update() {
		// Compute time difference between current and last frame
		var deltaTime = Scheduler.time() - lastTime;
		lastTime = Scheduler.time();

		// fps
		lastRealTime = realTime;
		realTime = Scheduler.realTime();
		fps = 1.0 / (realTime - lastRealTime);

		// region camera controls
		// Compute new orientation
		if (isMouseDown) {
			horizontalAngle += mouseSpeed * mouseDeltaX * -1;
			verticalAngle += mouseSpeed * mouseDeltaY * -1;
		}

		// Direction : Spherical coordinates to Cartesian coordinates conversion
		var direction = new FastVector3(Math.cos(verticalAngle) * Math.sin(horizontalAngle), Math.sin(verticalAngle),
			Math.cos(verticalAngle) * Math.cos(horizontalAngle));

		// Right vector
		var right = new FastVector3(Math.sin(horizontalAngle - 3.14 / 2.0), 0, Math.cos(horizontalAngle - 3.14 / 2.0));

		// Up vector
		var up = right.cross(direction);

		// Movement
		if (moveForward) {
			var v = direction.mult(deltaTime * speed);
			cameraPos = cameraPos.add(v);
		}
		if (moveBackward) {
			var v = direction.mult(deltaTime * speed * -1);
			cameraPos = cameraPos.add(v);
		}
		if (moveUp) {
			var v = up.mult(deltaTime * speed);
			cameraPos = cameraPos.add(v);
		}
		if (moveDown) {
			var v = up.mult(deltaTime * speed * -1);
			cameraPos = cameraPos.add(v);
		}
		if (strafeRight) {
			var v = right.mult(deltaTime * speed);
			cameraPos = cameraPos.add(v);
		}
		if (strafeLeft) {
			var v = right.mult(deltaTime * speed * -1);
			cameraPos = cameraPos.add(v);
		}

		// Look vector
		var look = cameraPos.add(direction);
		// Camera matrix
		view = FastMatrix4.lookAt(cameraPos, // Camera is here
			look, // and looks here : at the same cameraPos, plus "direction"
			up // Head is up (set to (0, -1, 0) to look upside-down)
		);
		// end region

		// Update model-view-projection matrix
		mvp = FastMatrix4.identity();
		mvp = mvp.multmat(projection);
		mvp = mvp.multmat(view);
		mvp = mvp.multmat(model);

		mouseDeltaX = 0;
		mouseDeltaY = 0;
	}

	public function render(framebuffer:Array<Framebuffer>) {
		var g2 = framebuffer[0].g2;
		var g4 = framebuffer[0].g4;

		// Begin rendering
		g4.begin();
		g4.clear(Color.fromValue(Reference.COLOR_BG), 1.0); // clear

		g4.setPipeline(pipeline);

		g4.setMatrix(mvpID, mvp);
		g4.setMatrix(modelMatrixID, model);
		g4.setMatrix(viewMatrixID, view);

		// Set light cameraPos to (0, 4, 0)
		// g4.setFloat3(lightID, Math.sin(Scheduler.time()) * 5, 4, Math.cos(Scheduler.time()) * 5);

		// direct light struct
		g4.setVector3(dirLightDirID, new FastVector3(-0.2, -1.0, -0.3));
		g4.setVector3(dirLightAmbID, new FastVector3(0.3, 0.3, 0.3));
		g4.setVector3(dirLightDiffID, new FastVector3(0.25, 0.25, 0.25));
		g4.setVector3(dirLightSpecID, new FastVector3(0.5, 0.5, 0.5));

		// material struct
		g4.setTexture(matDiffID, image_uv);
		g4.setTexture(matSpecID, image_uv);
		g4.setFloat(matShinID, 32.0);

		// reset model translation, rotation and scale
		g4.setMatrix(translateID, FastMatrix4.translation(0, 0, 0));
		g4.setMatrix(rotateID, FastMatrix4.rotation(0, 0, 0));
		g4.setMatrix(scaleID, FastMatrix4.scale(1, 1, 1));

		// todo empty texture/material

		g4.setFloat(timeID, Scheduler.time());
		g4.setVector3(viewPosID, cameraPos);

		for (obj in ObjectManager.get()) {
			g4.setMatrix(translateID, FastMatrix4.translation(obj.translate.x, obj.translate.y, obj.translate.z));
			g4.setMatrix(rotateID, FastMatrix4.rotation(obj.rotate.x, obj.rotate.y, obj.rotate.z));
			g4.setMatrix(scaleID, FastMatrix4.scale(obj.scale.x, obj.scale.y, obj.scale.z));

			g4.setTexture(textureID, image_uv);

			// for ([mesh_vbo in obj.getVertexBuffers(), mesh_ibo in obj.getIndexBuffers()]) {
			// 	for ([prim_vbo in mesh_vbo, prim_ibo in mesh_ibo]) {
			// 		g4.setVertexBuffer(prim_vbo);
			// 		g4.setIndexBuffer(prim_ibo);

			// 		g4.drawIndexedVertices();
			// 	}
			// }
			g4.setVertexBuffer(obj.getVertexBuffers());
			g4.setIndexBuffer(obj.getIndexBuffers());

			g4.drawIndexedVertices();
		}

		// End rendering
		g4.end();

		// region ui
		ui.begin(g2);
		if (ui.window(Id.handle(), 0, 0, Std.int(Reference.SCREEN_WIDTH * .3), Std.int(Reference.SCREEN_HEIGHT * .3), true)) {
			// ui.text("devil");
			// object_devil.translate = new Vector3(
			// 	ui.slider(Id.handle(), "translate x", -100, 100, false, 100),
			// 	ui.slider(Id.handle(), "translate y", -100, 100, false, 100),
			// 	ui.slider(Id.handle(), "translate z", -100, 100, false, 100)
			// );
			// object_devil.rotate = new Vector3(
			// 	ui.slider(Id.handle(), "rotate x", -360, 360, false, 1) * Math.PI/180,
			// 	ui.slider(Id.handle(), "rotate y", -360, 360, false, 1) * Math.PI/180,
			// 	ui.slider(Id.handle(), "rotate z", -360, 360, false, 1) * Math.PI/180
			// );
			// object_devil.scale = new Vector3(
			// 	ui.slider(Id.handle({value: 1}), "scale x", 1, 10, false, 1),
			// 	ui.slider(Id.handle({value: 1}), "scale y", 1, 10, false, 1),
			// 	ui.slider(Id.handle({value: 1}), "scale z", 1, 10, false, 1)
			// );
		}

		var cam_handle = Id.handle();
		cam_handle.redraws = 1; // force udpate every frame
		if (ui.window(cam_handle, 0, 0, Std.int(Reference.SCREEN_WIDTH * .3), Std.int(Reference.SCREEN_HEIGHT * .3), true)) {
			ui.text("Camera");
			ui.text('X: ${cameraPos.x}');
			ui.text('Y: ${cameraPos.y}');
			ui.text('Z: ${cameraPos.z}');
			if (ui.button("Reset cam"))
				resetCam();
			ui.text('FPS: ${Math.round(fps)}');
		}

		if (ui.window(Id.handle(), 540, 10, 240, 200, true)) {
			var htab = Id.handle({position: 0});
			if (ui.tab(htab, "Tab 1")) {
				speedMod = ui.slider(Id.handle({value: 2}), "cam speed mod", 1, 10, false, 1);
				ui.button("B");
				ui.button("C");
			}
			if (ui.tab(htab, "Tab 2")) {
				ui.button("D");
				ui.button("E");
				ui.button("F");
				ui.button("G");
				ui.button("H");
				ui.button("J");
				ui.button("K");
				ui.button("L");
				ui.button("M");
				ui.button("N");
				ui.button("O");
				ui.button("P");
				ui.button("Q");
				ui.button("R");
				ui.button("S");
			}
			if (ui.tab(htab, "Another Tab")) {
				ui.text("Lorem ipsum dolor sit amet");
				ui.check(Id.handle(), "Check Box 1");
				ui.check(Id.handle(), "Check Box 2");
			}
		}
		ui.end();
		// end region

		update();
	}

	function onMouseDown(button:Int, x:Int, y:Int) {
		if (button == 1)
			isMouseDown = true;
	}

	function onMouseUp(button:Int, x:Int, y:Int) {
		if (button == 1)
			isMouseDown = false;
	}

	function onMouseMove(x:Int, y:Int, movementX:Int, movementY:Int) {
		mouseDeltaX = x - mouseX;
		mouseDeltaY = y - mouseY;

		mouseX = x;
		mouseY = y;
	}

	function onKeyDown(key:KeyCode) {
		if (key == KeyCode.Up || key == KeyCode.W)
			moveForward = true;
		else if (key == KeyCode.Down || key == KeyCode.S)
			moveBackward = true;
		else if (key == KeyCode.Space)
			moveUp = true;
		else if (key == KeyCode.Control)
			moveDown = true;
		else if (key == KeyCode.Left || key == KeyCode.A)
			strafeLeft = true;
		else if (key == KeyCode.Right || key == KeyCode.D)
			strafeRight = true;

		if (key == KeyCode.Shift)
			speed = speedBase * speedMod;
	}

	function onKeyUp(key:KeyCode) {
		if (key == KeyCode.Up || key == KeyCode.W)
			moveForward = false;
		else if (key == KeyCode.Down || key == KeyCode.S)
			moveBackward = false;
		else if (key == KeyCode.Space)
			moveUp = false;
		else if (key == KeyCode.Control)
			moveDown = false;
		else if (key == KeyCode.Left || key == KeyCode.A)
			strafeLeft = false;
		else if (key == KeyCode.Right || key == KeyCode.D)
			strafeRight = false;

		if (key == KeyCode.Shift)
			speed = speedBase;
	}

	function resetCam() {
		cameraPos = new FastVector3(0, 5, 7.5);
		horizontalAngle = 3.14;
		verticalAngle = 0.0;
	}
}
