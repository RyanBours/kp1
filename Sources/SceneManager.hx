package;

import kha.Image;

class Scene {
    public function new() {

    }

    public function onLoad() {

    }

    public function unLoad() {
        
    }

    public function update() {

    }

    public function render(backbuffer:Image) {

    }
}

class SceneManager {

    var scenes:Map<String, Scene> = [];
    var currentScene:Scene;

    public function new() {

    }

    public function update() {
        currentScene.update();
    }

	public function render(backbuffer:Image):Void {
        currentScene.render(backbuffer);
    }

    public function addScene(name:String, scene:Scene) {
        // scenes.push(scene);
        scenes.set(name, scene);
    }

    // public function setScene(?name:String, ?scene:Scene) {
    //     var new_scene = name == null ? new Scene() : scenes.get(name);
    //     var old_scene = currentScene;
    //     this.currentScene = new_scene;
    //     this.currentScene.onLoad();
    //     if (old_scene != null) old_scene.unLoad();
    // }

    public function setScene(scene:Scene) {
        this.currentScene = scene;
    }

    public function getScene() : Scene {
        return currentScene;
    }

}