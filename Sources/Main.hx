package;

import kha.Assets;
import kha.System;

class Main {
	public static function main() {
		System.start({
				title: "OBJ loader",
				width: Reference.SCREEN_WIDTH,
				height: Reference.SCREEN_HEIGHT,
				framebuffer: {samplesPerPixel: 16}
			}, function (_) {
				Assets.loadEverything(function () {
					// var project = new Project();
					// var project = new Test3D();
					var project = new TestObj3D();
					System.notifyOnFrames(project.render);
				});
			}
		);
	}
}
