package;

import kha.math.FastMatrix3;
import engine.Object;
import engine.ObjectManager;
import engine.helpers.GLTFModel;
import engine.helpers.ObjModel;

import gltf.GLTFLoader;

import kha.Assets;
import kha.Color;
import kha.Framebuffer;
import kha.Image;
import kha.Scheduler;
import kha.Shaders;
import kha.Window;

import kha.graphics4.CompareMode;
import kha.graphics4.ConstantLocation;
import kha.graphics4.CullMode;
import kha.graphics4.IndexBuffer;
import kha.graphics4.PipelineState;
import kha.graphics4.TextureUnit;
import kha.graphics4.Usage;
import kha.graphics4.VertexBuffer;
import kha.graphics4.VertexData;
import kha.graphics4.VertexStructure;

import kha.input.KeyCode;
import kha.input.Keyboard;
import kha.input.Mouse;

import kha.math.FastMatrix4;
import kha.math.FastVector3;
import kha.math.Vector3;

import zui.Id;
import zui.Zui;

@:tink
class TestObj3D {
	// region Variables
	var ui:Zui;

	// var vertexBuffer:VertexBuffer;
	// var indexBuffer:IndexBuffer;

	var pipeline:PipelineState;

	var mvp:FastMatrix4;
	var model:FastMatrix4;
	var view:FastMatrix4;
	var projection:FastMatrix4;

	// region shader ids
	var mvpID:ConstantLocation;
	var modelID:ConstantLocation;
	var viewID:ConstantLocation;
	var translateID:ConstantLocation;
	var rotateID:ConstantLocation;
	var scaleID:ConstantLocation;

	var timeID:ConstantLocation;

	var lightPosID:ConstantLocation;
	var camPosID:ConstantLocation;
	// end region

	// region time
	var lastTime = 0.0; // delta
	var realTime = Scheduler.realTime();
	var lastRealTime = Scheduler.realTime(); // fps
	var fps:Float;
	// end region

	// region camera
	var cameraPos:FastVector3 = new FastVector3(0, 5, 50); // Initial cameraPos: on +Z
	var horizontalAngle = 3.14; // Initial horizontal angle: toward -Z
	var verticalAngle = 0.0; // Initial vertical angle: none
	// end region

	// region controls
	var moveForward = false;
	var moveBackward = false;
	var moveUp = false;
	var moveDown = false;
	var strafeLeft = false;
	var strafeRight = false;
	var isMouseDown = false;

	var mouseX = 0.0;
	var mouseY = 0.0;
	var mouseDeltaX = 0.0;
	var mouseDeltaY = 0.0;

	var speedBase = 6.0;
	var speed = 6.0; // 6 units / second
	var speedMod = 4.;
	var mouseSpeed = 0.005;
	// end region

	// region systems
	var ObjectManager = new ObjectManager();
	// end region

	// end region

	public function new() {
		trace(Window.get(0).title = "test"); // BUG: Window.get(0).title returns Kha

		// region structure
		var structure = new VertexStructure();
		structure.add("pos", VertexData.Float3);
		structure.add("uv", VertexData.Float2);
		structure.add("nor", VertexData.Float3);
		// end region

		// region pipeline
		pipeline = new PipelineState();
		pipeline.inputLayout = [structure];
		pipeline.vertexShader = Shaders.obj_vert;
		pipeline.fragmentShader = Shaders.obj_frag;
		pipeline.depthWrite = true;
		pipeline.depthMode = CompareMode.Less;
		pipeline.cullMode = CullMode.Clockwise;
		pipeline.compile();
		// end region

		// region shader ids
		mvpID = pipeline.getConstantLocation("MVP");
		modelID = pipeline.getConstantLocation("model");
		viewID = pipeline.getConstantLocation("view");
		translateID = pipeline.getConstantLocation("translate");
		rotateID = pipeline.getConstantLocation("rotate");
		scaleID = pipeline.getConstantLocation("scale");

		// timeID = pipeline.getConstantLocation("u_time");

		lightPosID = pipeline.getConstantLocation("lightPos");
		camPosID = pipeline.getConstantLocation("camPos");

		// end region

		// region MVP calculations
		model = FastMatrix4.identity();
		projection = FastMatrix4.perspectiveProjection(45., Reference.SCREEN_WIDTH / Reference.SCREEN_HEIGHT, .1, 100.);
		view = FastMatrix4.lookAt(new FastVector3(4, 3, 3), new FastVector3(0, 0, 0), new FastVector3(0, 1, 0));

		mvp = FastMatrix4.identity();
		mvp = mvp.multmat(projection);
		mvp = mvp.multmat(view);
		mvp = mvp.multmat(model);
		// end region

		// regions scene objects
		var model = GLTFLoader.load(Assets.blobs.ship_wreck2_gltf.toString());
		// var model = GLTFLoader.load(Assets.blobs.dodecadodecahedron_gltf.toString());
		var model_wrapper = new GLTFModel(model, structure);
		var scene_object = new Object(model_wrapper.createMesh());
		scene_object.scale = new kha.math.Vector3(4, 4, 4);
		ObjectManager.add(scene_object);

		var model = new ObjModel(Assets.blobs.obj_dodecadodecahedron_obj.toString());
		// var model = new ObjModel(Assets.blobs.obj_cube_obj.toString());
		var scene_object = new Object(model.createMesh());
		scene_object.translate = new kha.math.Vector3(20, 10, 0);
		scene_object.scale = new kha.math.Vector3(.5, .5, .5);
		ObjectManager.add(scene_object);

		// end region

		// assign event handlers
		Mouse.get().notify(onMouseDown, onMouseUp, onMouseMove, null);
		Keyboard.get().notify(onKeyDown, onKeyUp);

		// calculate delta time
		lastTime = Scheduler.time();

		// init ui
		ui = new Zui({font: Assets.fonts.HackRegular});
	}

	public function update() {
		// Compute time difference between current and last frame
		var deltaTime = Scheduler.time() - lastTime;
		lastTime = Scheduler.time();

		// fps
		lastRealTime = realTime;
		realTime = Scheduler.realTime();
		fps = 1.0 / (realTime - lastRealTime);

		// region camera controls
		// Compute new orientation
		if (isMouseDown) {
			horizontalAngle += mouseSpeed * mouseDeltaX * -1;
			verticalAngle += mouseSpeed * mouseDeltaY * -1;
		}

		// Direction : Spherical coordinates to Cartesian coordinates conversion
		var direction = new FastVector3(Math.cos(verticalAngle) * Math.sin(horizontalAngle), Math.sin(verticalAngle),
			Math.cos(verticalAngle) * Math.cos(horizontalAngle));

		// Right vector
		var right = new FastVector3(Math.sin(horizontalAngle - 3.14 / 2.0), 0, Math.cos(horizontalAngle - 3.14 / 2.0));

		// Up vector
		var up = right.cross(direction);

		// Movement
		if (moveForward) {
			var v = direction.mult(deltaTime * speed);
			cameraPos = cameraPos.add(v);
		}
		if (moveBackward) {
			var v = direction.mult(deltaTime * speed * -1);
			cameraPos = cameraPos.add(v);
		}
		if (moveUp) {
			var v = up.mult(deltaTime * speed);
			cameraPos = cameraPos.add(v);
		}
		if (moveDown) {
			var v = up.mult(deltaTime * speed * -1);
			cameraPos = cameraPos.add(v);
		}
		if (strafeRight) {
			var v = right.mult(deltaTime * speed);
			cameraPos = cameraPos.add(v);
		}
		if (strafeLeft) {
			var v = right.mult(deltaTime * speed * -1);
			cameraPos = cameraPos.add(v);
		}

		// Look vector
		var look = cameraPos.add(direction);
		// Camera matrix
		view = FastMatrix4.lookAt(cameraPos, // Camera is here
			look, // and looks here : at the same cameraPos, plus "direction"
			up // Head is up (set to (0, -1, 0) to look upside-down)
		);
		// end region

		// Update model-view-projection matrix
		mvp = FastMatrix4.identity();
		mvp = mvp.multmat(projection);
		mvp = mvp.multmat(view);
		mvp = mvp.multmat(model);

		mouseDeltaX = 0;
		mouseDeltaY = 0;
	}

	public function render(framebuffer:Array<Framebuffer>) {
		var g2 = framebuffer[0].g2;
		var g4 = framebuffer[0].g4;

		// Begin rendering
		g4.begin();
		g4.setPipeline(pipeline);

		g4.setMatrix(mvpID, mvp);
		g4.setMatrix(modelID, model);
		g4.setMatrix(viewID, view);

		// g4.setFloat(timeID, Scheduler.time());

		// region reset
		g4.clear(Color.fromValue(Reference.COLOR_BG), 1.0); 
		g4.setMatrix(translateID, FastMatrix4.translation(0, 0, 0));
		g4.setMatrix(rotateID, FastMatrix4.rotation(0, 0, 0));
		g4.setMatrix(scaleID, FastMatrix4.scale(1, 1, 1));
		// end region

		// region lighting
		// g4.setVector3(lightPosID, new FastVector3(0, 10, 0));
		g4.setFloat3(lightPosID, Math.sin(Scheduler.time()) * 30, 8, Math.cos(Scheduler.time()) * 30);
		// end region

		// region camera
		g4.setVector3(camPosID, cameraPos);
		// end region

		// region objects
		for (obj in ObjectManager.get()) {
			g4.setMatrix(translateID, FastMatrix4.translation(obj.translate.x, obj.translate.y, obj.translate.z));
			g4.setMatrix(rotateID, FastMatrix4.rotation(obj.rotate.x, obj.rotate.y, obj.rotate.z));
			g4.setMatrix(scaleID, FastMatrix4.scale(obj.scale.x, obj.scale.y, obj.scale.z));

			// for ([mesh_vbo in obj.getVertexBuffers(), mesh_ibo in obj.getIndexBuffers()]) {
			// 	for ([prim_vbo in mesh_vbo, prim_ibo in mesh_ibo]) {
			// 		g4.setVertexBuffer(prim_vbo);
			// 		g4.setIndexBuffer(prim_ibo);

			// 		g4.drawIndexedVertices();
			// 	}
			// }

			g4.setVertexBuffer(obj.getVertexBuffers());
			g4.setIndexBuffer(obj.getIndexBuffers());

			g4.drawIndexedVertices();
		}
		// end region

		g4.end();

		// region ui
		ui.begin(g2);
		if (ui.window(Id.handle(), Std.int(Reference.SCREEN_WIDTH * .3), 0, Std.int(Reference.SCREEN_WIDTH * .3), Std.int(Reference.SCREEN_HEIGHT * .3), true)) {
			// FIXME: Controls all objects
			// for (obj in ObjectManager.get()) {
			// 	ui.text('todo obj name');
			// 	obj.translate = new Vector3(
			// 		ui.slider(Id.handle(), "translate x", -100, 100, false, 100),
			// 		ui.slider(Id.handle(), "translate y", -100, 100, false, 100),
			// 		ui.slider(Id.handle(), "translate z", -100, 100, false, 100)
			// 	);
			// 	obj.rotate = new Vector3(
			// 		ui.slider(Id.handle(), "rotate x", -360, 360, false, 1) * Math.PI/180,
			// 		ui.slider(Id.handle(), "rotate y", -360, 360, false, 1) * Math.PI/180,
			// 		ui.slider(Id.handle(), "rotate z", -360, 360, false, 1) * Math.PI/180
			// 	);
			// 	obj.scale = new Vector3(
			// 		ui.slider(Id.handle({value: 1}), "scale x", 1, 10, false, 1),
			// 		ui.slider(Id.handle({value: 1}), "scale y", 1, 10, false, 1),
			// 		ui.slider(Id.handle({value: 1}), "scale z", 1, 10, false, 1)
			// 	);
			// }
		}

		var cam_handle = Id.handle();
		cam_handle.redraws = 1; // force udpate every frame
		if (ui.window(cam_handle, 0, 0, Std.int(Reference.SCREEN_WIDTH * .3), Std.int(Reference.SCREEN_HEIGHT * .3), true)) {
			ui.text('FPS: ${Math.round(fps)}');
			ui.text("Camera");
			ui.text('X: ${cameraPos.x}');
			ui.text('Y: ${cameraPos.y}');
			ui.text('Z: ${cameraPos.z}');
			if (ui.button("Reset cam")) resetCam();
			speedMod = ui.slider(Id.handle({value: 2}), "cam speed mod", 1, 10, false, 1);
		}

		if (ui.window(Id.handle(), Std.int(Reference.SCREEN_WIDTH * .6), 0, Std.int(Reference.SCREEN_WIDTH * .3), Std.int(Reference.SCREEN_HEIGHT * .3), true)) {
			var htab = Id.handle({position: 0});
			if (ui.tab(htab, "Ambient")) {
				
			}
			if (ui.tab(htab, "Tab 2")) {
			}
		}
		ui.end();
		// end region

		update();
	}


	// region control event handlers
	function onMouseDown(button:Int, x:Int, y:Int) {
		if (button == 1)
			isMouseDown = true;
	}

	function onMouseUp(button:Int, x:Int, y:Int) {
		if (button == 1)
			isMouseDown = false;
	}

	function onMouseMove(x:Int, y:Int, movementX:Int, movementY:Int) {
		mouseDeltaX = x - mouseX;
		mouseDeltaY = y - mouseY;

		mouseX = x;
		mouseY = y;
	}

	function onKeyDown(key:KeyCode) {
		if (key == KeyCode.Up || key == KeyCode.W)
			moveForward = true;
		else if (key == KeyCode.Down || key == KeyCode.S)
			moveBackward = true;
		else if (key == KeyCode.Space)
			moveUp = true;
		else if (key == KeyCode.Control)
			moveDown = true;
		else if (key == KeyCode.Left || key == KeyCode.A)
			strafeLeft = true;
		else if (key == KeyCode.Right || key == KeyCode.D)
			strafeRight = true;

		if (key == KeyCode.Shift)
			speed = speedBase * speedMod;
	}

	function onKeyUp(key:KeyCode) {
		if (key == KeyCode.Up || key == KeyCode.W)
			moveForward = false;
		else if (key == KeyCode.Down || key == KeyCode.S)
			moveBackward = false;
		else if (key == KeyCode.Space)
			moveUp = false;
		else if (key == KeyCode.Control)
			moveDown = false;
		else if (key == KeyCode.Left || key == KeyCode.A)
			strafeLeft = false;
		else if (key == KeyCode.Right || key == KeyCode.D)
			strafeRight = false;

		if (key == KeyCode.Shift)
			speed = speedBase;
	}

	function resetCam() {
		cameraPos = new FastVector3(0, 5, 7.5);
		horizontalAngle = 3.14;
		verticalAngle = 0.0;
	}
	// end region
}
