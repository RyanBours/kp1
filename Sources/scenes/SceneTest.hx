package scenes;

import kha.System;
import kha.Scaler;
import kha.Assets;
import kha.Color;
import kha.Image;
import kha.Framebuffer;
import SceneManager.Scene;

class SceneTest extends Scene {
    var title = 'scene test';

	var backbuffer:Image;

	public function new() {
		super();
        trace('sceneTest init');
	}

	override public function onLoad() {
		trace('scene test onload');
	}

	override public function unLoad() {
		trace('scene test unload');
	}

	override public function update() {
        trace('SceneTest update');
    }

	override public function render(backbuffer: Image) {
        var g2 = backbuffer.g2;
		var g4 = backbuffer.g4;

        g2.color = Color.White; // Reset Color to White
		g2.fontSize = 28;
		g2.font = Assets.fonts.HackRegular;
		g2.fillRect(0, 0, 10, 10); // top left
		g2.drawString('Lorem Ipsum sample text:!@#$%^&*()?/<>-_=+~\\|\'., ', 0, 490);
        g2.color = Color.Red;
        g2.drawString('Scene Test', 0, 300);

		g4.begin();
		g4.end();
    }
}
