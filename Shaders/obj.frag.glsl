#version 330 core
in vec3 Normal;
in vec3 FragPos;

uniform vec3 lightPos;
uniform vec3 cameraPos;

out vec4 FragColor;

void main()
{
    // calc ambient
    float ambientStrength = 0.8;
    vec3 lightColor = vec3(0.8, 0.2, 0.4);
    vec3 ambient = ambientStrength * lightColor;

    // calc diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    
    // calc specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(cameraPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 8);
    vec3 specular = specularStrength * spec * lightColor; 

    // calc phong
    vec3 materialColor = vec3(1.0, 1.0, 1.0);
    vec3 result = (
        ambient
        + diffuse
        + specular
        ) * materialColor;
    FragColor = vec4(result, 1.0);
    // FragColor = vec4(gl_FragCoord.z);
}