#version 450

struct Material{
	sampler2D diffuse;
	sampler2D specular;
	float shininess;
};
uniform Material material;

struct DirLight{
	vec3 direction;
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};
uniform DirLight dirLight;

in vec3 pos;
in vec2 vUV;
in vec3 vNor;
in vec3 fragPos;

uniform sampler2D myTextureSampler;
uniform float u_time;
uniform vec3 viewPos;

out vec4 fragColor;

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir) {
	    vec3 lightDir = normalize(-light.direction);
	    // diffuse shading
	    float diff = max(dot(normal, lightDir), 0.0);
	    // specular shading
	    vec3 reflectDir = reflect(-lightDir, normal);
	    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	    // combine results
	    vec3 ambient  = light.ambient  * vec3(texture(material.diffuse, vUV));
	    vec3 diffuse  = light.diffuse  * diff * vec3(texture(material.diffuse, vUV));
	    vec3 specular = light.specular * spec * vec3(texture(material.specular, vUV));
	    return (ambient + diffuse + specular);
}  

void main(){
	vec4 tex = texture(myTextureSampler, vUV);

	vec3 norm = normalize(vNor);
	vec3 viewDir = normalize(viewPos - fragPos);

	vec3 result = CalcDirLight(dirLight, norm, viewDir);
	fragColor = tex * vec4(result, 1.0);
}