#version 450

// Input vertex data, different for all executions of this shader
layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 nor;

// Output data: will be interpolated for each fragment
out vec2 vUV;
out vec3 vNor;
out vec3 fragPos;

// Values that stay constant for the whole mesh
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;

uniform mat4 translate;
uniform mat4 rotate;
uniform mat4 scale;

void main() {

    mat4 TRS = translate * rotate * scale;

	gl_Position = MVP * TRS * vec4(pos, 1.0);

	vNor = nor;
	vUV = uv;

	fragPos = vec3(M * vec4(pos, 1.0));
}
