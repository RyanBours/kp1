#version 330 core
in vec3 pos;
in vec2 uv;
in vec3 nor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 MVP;

uniform mat4 translate;
uniform mat4 rotate;
uniform mat4 scale;

out vec3 Normal;
out vec3 FragPos;

void main()
{
    Normal = mat3(transpose(inverse(model))) * nor;
    // Normal = nor;
    mat4 TRS = translate * rotate * scale;
    FragPos = vec3(model * TRS * vec4(pos, 1.0));
    gl_Position = MVP * TRS * vec4(pos, 1.0);
} 